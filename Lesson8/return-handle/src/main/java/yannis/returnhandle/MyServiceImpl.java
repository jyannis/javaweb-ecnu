package yannis.returnhandle;

import org.springframework.stereotype.Service;

@Service
public class MyServiceImpl implements MyService {
    @Override
    public String url() {
        return "Hello world!";
    }
}
