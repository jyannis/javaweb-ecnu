package yannis.returnhandle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReturnHandleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReturnHandleApplication.class, args);
	}

}
