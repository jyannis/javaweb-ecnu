package yannis.loghandle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogHandleApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogHandleApplication.class, args);
	}

}
