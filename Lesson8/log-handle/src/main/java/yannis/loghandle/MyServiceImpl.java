package yannis.loghandle;

import org.springframework.stereotype.Service;

@Service
public class MyServiceImpl implements MyService {
    @Override
    public String url() {
        throw new MyException(EnumExceptionType.PARAM_ILLEGAL);
    }
}
