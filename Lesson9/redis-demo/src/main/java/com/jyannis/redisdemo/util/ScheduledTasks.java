package com.jyannis.redisdemo.util;

import com.jyannis.redisdemo.mapper.PostingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class ScheduledTasks {

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    PostingMapper postingMapper;

    //每隔2s执行一次
    @Scheduled(cron = "0 0 0 * * ?")
    public void reportCurrentTime() {
        Integer likes;
        try{
            //从redis中取出键为LIKE_KEY(Integer id)的值
            likes = (Integer)redisUtil.get("redisdemo:likes:1");
            if(likes != null){
                postingMapper.setLikesByPrimaryKey(likes,1);
            }
        }catch (Exception e){
        }
    }

}
