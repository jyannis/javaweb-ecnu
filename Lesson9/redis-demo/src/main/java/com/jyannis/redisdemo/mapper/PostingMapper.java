package com.jyannis.redisdemo.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface PostingMapper {

    @Select("SELECT likes FROM posting WHERE id=#{id}")
    Integer getLikesByPrimaryKey(Integer id);

    @Update("UPDATE posting SET likes=#{likes} WHERE id=#{id}")
    void setLikesByPrimaryKey(@Param("likes")Integer likes,@Param("id")Integer id);

}
