package com.jyannis.redisdemo.controller;

import com.jyannis.redisdemo.service.LikesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/likes")
@Validated
public class LikesController {

    @Autowired
    LikesService likesService;

    @GetMapping("/{id}")
    public int getLikes(@NotNull @PathVariable Integer id){
        return likesService.getLikes(id);
    }

    @PostMapping("/{id}")
    public int likes(@NotNull @PathVariable Integer id){
        likesService.likes(id);
        return 0;
    }

}
