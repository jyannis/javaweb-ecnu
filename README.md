# JavaWeb后端学习小组

#### 介绍
华东师范大学JavaWeb后端短期课程学习资源
  
Lesson7及后续内容的文本资料已移到知乎专栏
  
https://zhuanlan.zhihu.com/c_1190573071407575040
  
  
  

#### 内容
  
**Lesson1** 环境搭建 + 软件介绍 + idea使用入门
  
**Lesson2** Java答疑（多态、注解） + MySQL语法基础 + Navicat可视化工具使用入门
  
**Lesson3** B/S架构简介 + HTTP基础 + 服务器对于HTTP请求的处理流程 + 会话基本概念
  
**Lesson4** Maven基础 + 第一个SpringBoot程序 + IOC概念入门 + 写一个简易登录模块
  
**Lesson5** MyBatis概念 + 传统JDBC实践及其缺陷 + MyBatis基于XML实践 + MyBatis基于注解实践 + 通用Mapper + 修改login-demo数据访问层（作业）
  
**Lesson6** JSON数据格式 + GSON库 + Controller层常用的接收参数方式 + Validation数据校验(常用校验注解和自定义校验注解)
  
**Lesson7** 集成swagger2实现接口文档 + 使用postman进行接口测试
  
**Lesson8** 统一返回封装 + 统一异常处理 + 统一日志处理
  