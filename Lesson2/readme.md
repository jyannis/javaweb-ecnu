课前预告：

参考  **Lesson2 Java+MySQL.pdf** 


___
课后整理：

文档：Lesson2.1 Java答疑.note
链接：http://note.youdao.com/noteshare?id=1960eaf0180ad13c8cb1ca3d5fecf896

文档：Lesson2.2 MySQL入门.note
链接：http://note.youdao.com/noteshare?id=d654f92181a81362fd9e10a5385fa9da&sub=F54972E734BD4FB6B50CD650B8A3605E

___
项目源码：

Java多态demo：参考 **PolymorphicTest** 


Java注解demo：参考 **AnnotationTest** 

___
作业：

MySQL基础测验：https://www.w3school.com.cn/quiz/quiz.asp?quiz=sql

完成后截图上传到issue的 **Lesson2作业**中（评论该话题即可），记得注明姓名