import java.util.*;

//在该类中使用注解修饰方法
public class PasswordUtils {
	@UseCase(id = 47, description = "Passwords must contain at least one numeric")
	public Boolean validatePassword(String password) {
		return (password.matches("\\w*\\d\\w*"));
	}

	@UseCase(id = 48)
	public String encryptPassword(String password) {
		return new StringBuilder(password).reverse().toString();
	}

	@UseCase(id = 49, description = "New passwords can’t equal previously used ones")
	public Boolean checkForNewPassword(List<String> prevPasswords, String password) {
		return !prevPasswords.contains(password);
	}
}