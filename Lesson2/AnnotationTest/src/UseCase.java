import java.lang.annotation.*;

//自定义注解UseCase
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface UseCase{
	public int id();
	public String description() default "no description";
}
