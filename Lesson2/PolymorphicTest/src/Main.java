public class Main {
    public static void main(String[] args) {
        Fruit a = new Apple();
        Fruit b = new Banana();
        a.sell();
        b.sell();
    }
}
