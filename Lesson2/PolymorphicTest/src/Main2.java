public class Main2 {
    public static void main(String[] args) {
        //检查库存，把库存里的商品提取出来
        Fruit a = new Apple();
        Fruit b = new Banana();

        //真正的卖商品请求转发给下一层
//        a.sell();
//        b.sell();
        SellUtil.sell(new Fruit[]{a,b});
    }
}
