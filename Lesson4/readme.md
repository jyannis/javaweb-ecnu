课前预告：

参考  **Lesson4 SpringBoot初探.pdf** 


___
课后整理：

文档：Lesson4 SpringBoot初探.note
链接：http://note.youdao.com/noteshare?id=9f34d188c70429486de6b7a90dd7e50c&sub=A868A0A8F9A64DFBB30F9AEBF4AAAF0A

___
项目源码：

maven项目构建：参考 **maven-demo** 


第一个springboot程序：参考 **springboot-helloworld**


简易登录模块：参考 **login-demo**


bean注入方式：参考 **spring-bean-demo**