package undestiny.springbeandemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class MyController {

    //@Autowired默认按类型注入，如果同一类型有多个bean就报错
    @Autowired
    private Integer autowire_bean;

    //@Resource支持name注入或type注入，或同时指定两者注入（需同时满足name和type）
    @Resource(name = "resource_name1")
    private String resource_bean1;

    @Resource(name = "resource_name2",type = String.class)
    private String resource_bean2;



    @GetMapping("/autowire_bean")
    public String getAutowire_bean(){
        return "autowire_bean: " + autowire_bean + "<br>resource_bean1: "
                + resource_bean1 + "<br>resource_bean2: " + resource_bean2;
    }

}
