package undestiny.logindemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import undestiny.logindemo.service.UserService;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/getUsernames")
    public List<String> getUsernames(HttpSession session){
        //检查是否登录（session是否存在）
        if(session.getAttribute("user") != null) {
            return userService.getUsernames();
        }
        return null;
    }

}
