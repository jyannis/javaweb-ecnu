package undestiny.logindemo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import undestiny.logindemo.dao.UserDao;
import undestiny.logindemo.entity.User;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    public UserServiceImpl() {
        log.info("call new UserServiceImpl()");
    }

    @Override
    public User getUser(String username,String password) {
        User user = userDao.getUserByUsernameAndPassword(username,password);
        if(user == null)throw new RuntimeException();
        return user;
    }

    @Override
    public List<String> getUsernames() {
        return new ArrayList<>(userDao.getUsernameList());
    }
}
