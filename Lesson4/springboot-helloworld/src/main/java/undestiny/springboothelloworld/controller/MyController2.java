package undestiny.springboothelloworld.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MyController2 {

    @GetMapping("/myHtml")
    public String myHtml(){
        return "hello.html";
    }

}
