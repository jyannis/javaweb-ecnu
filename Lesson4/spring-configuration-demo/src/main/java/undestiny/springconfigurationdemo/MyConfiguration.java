package undestiny.springconfigurationdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

@Configuration
//@Component
//@RestController
//@Service
//@Repository
public class MyConfiguration {

    @Bean
    public A a(){
        return new A();
    }

    @Bean
    public B b(){
        a();
        return new B();
    }


    /**
     * 在spring容器初始化时，会对@Configuration修饰的类进行“增强”（动态代理），
     * 对于实例域和方法都会进行修改（比如把b()里进行重复初始化的a()去掉。
     * 而@Component，@RestController，@Service，@Repository则不会进行增强。
     * 可以通过debug来验证（在容器初始化的位置打断点）
     */


}
