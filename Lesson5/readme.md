课前预告：

参考  **Lesson5 SpringBoot集成Mybatis.pdf** 


___
课后整理：

文档：Lesson5 SpringBoot集成MyBatis.note
链接：http://note.youdao.com/noteshare?id=1f3696aeed4e6b6fa4abbee7f02a3acc&sub=1A5674D050404D76895A3299321F1EA5

___
项目源码：

jdbc使用demo（普通statement + 预编译preparedStatement）：参考 **jdbc-demo** 


mybatis基于XML映射文件配置demo：参考 **springboot-mybatis**


mybatis基于注解配置demo：参考 **springboot-mybatis-annotation**


通用mapper配置demo：参考 **springboot-mybatis-tk**

___
作业：

修改Lesson4中的**login-demo**，用Mybatis来替换Dao层。使用XML方式、注解方式均可，也可以使用通用Mapper。

完成后上传到自己的github或gitee上，并把引用链接复制上传到issue的 **Lesson5作业** 中（评论该话题即可），记得注明姓名