package undestiny.springbootmybatistk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import undestiny.springbootmybatistk.entity.User;
import undestiny.springbootmybatistk.mapper.UserMapper;

@Service
public class UserService {

    @Autowired
    UserMapper userMapper;

    void test(){
        userMapper.insert(new User());
    }

}
