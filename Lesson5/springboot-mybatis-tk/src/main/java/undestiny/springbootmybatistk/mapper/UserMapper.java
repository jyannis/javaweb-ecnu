package undestiny.springbootmybatistk.mapper;

import org.apache.ibatis.annotations.Mapper;
import undestiny.springbootmybatistk.entity.User;

@Mapper
public interface UserMapper extends MyMapper<User> {

}
