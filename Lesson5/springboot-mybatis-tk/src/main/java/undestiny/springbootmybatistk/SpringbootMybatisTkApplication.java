package undestiny.springbootmybatistk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMybatisTkApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootMybatisTkApplication.class, args);
	}

}
