package undestiny.springbootmybatistk.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import undestiny.springbootmybatistk.entity.User;
import undestiny.springbootmybatistk.mapper.UserMapper;

import java.util.List;

@RestController
@Slf4j
public class Controller {

    @Autowired
    UserMapper userMapper;

    @GetMapping("/insertUser/{username}/{password}")
    public Long insertUser(@PathVariable("username")String username,
                           @PathVariable("password")String password){
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);

        //insertUser返回插入的条数
        int count = userMapper.insert(user);
        log.info("count=" + count);
        log.info("id=" + user.getId());
        return user.getId();
    }

    @GetMapping("/getUserById/{id}")
    public User getUserById(@PathVariable("id")Long id){
        User user = User.builder().id(id).build();
        return (User)userMapper.selectOne(user);
    }

    @GetMapping("/getUserByUsername/{username}")
    public List<User> getUserByUsername(@PathVariable("username")String username){
        User user = User.builder().username(username).build();
        return userMapper.select(user);
    }

    @GetMapping("/deleteUserById/{id}")
    public int deleteUserById(@PathVariable("id")Long id){
        return userMapper.deleteByPrimaryKey(id);
    }
}
