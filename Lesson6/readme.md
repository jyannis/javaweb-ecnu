课前预告：

参考  **Lesson6 数据传输与校验.pdf** 


___
课后整理：

文档：Lesson6 数据传输与校验.note
链接：http://note.youdao.com/noteshare?id=6ce5d633cf27e0378e81c99e168cb997&sub=CB3F83E63F434705B74431DC65F8E39C

___
项目源码：

使用Gson来做json对象-java对象之间的转换：参考 **gson-demo** 


controller几种常用收参方式：参考 **transfer-demo**


数据（参数）校验javax.validation：参考 **validation-demo**
