package undestiny.gsondemo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    @Bean
    public Gson myGson(){
        return new GsonBuilder().serializeNulls().create();
    }

}
