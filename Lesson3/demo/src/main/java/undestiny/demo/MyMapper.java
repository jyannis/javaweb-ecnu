package undestiny.demo;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface MyMapper {

    @Select("select Host from db limit 0,1")
    String get();

}
