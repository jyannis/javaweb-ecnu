package undestiny.cookiedemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
public class SessionController {

    @GetMapping("/setSession")
    public String setSession(HttpSession session){
        User user = new User();
        user.setUsername("Jack");
        user.setPassword("asdf");
        session.setAttribute("user",user);
        //当服务器端首次往session中存储值时(session.setAttribute(name, value))，
        // 服务器端(Tomcat)会自动向响应头(Response Head)中增加一个Set-Cookie的头值为JSESSIONID的键值对
        return "SUCCESS";
    }

    @GetMapping("/getSession")
    public User getSession(HttpSession session){
        return (User)session.getAttribute("user");
    }

    @GetMapping("/logout")
    public String logout(HttpSession session){
        //注销session（在服务器里删除该session）
        session.invalidate();
        return "Logout successfully";
    }
}
